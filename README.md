# isoserv

Create the /data directory

```bash
$ sudo mkdir /data
```

Set the correct permissions on the data directory

```bash
$ sudo chmod ...
$ sudo chown ...
```

Clone the repository to your directory of choice

```bash
$ git clone https://gitlab.com/jsachnet/isoserv.git
Cloning into 'isoserv'...
remote: Enumerating objects: 10, done.
remote: Counting objects: 100% (10/10), done.
remote: Compressing objects: 100% (7/7), done.
remote: Total 10 (delta 1), reused 0 (delta 0), pack-reused 0
Unpacking objects: 100% (10/10), 1008 bytes | 336.00 KiB/s, done.
```

Start the container

```bash
$ cd isoserv
$ docker-compose up -d
Creating network "isoserv_default" with the default driver
Pulling iso (nginx:latest)...
latest: Pulling from library/nginx
afb6ec6fdc1c: Pull complete
b90c53a0b692: Pull complete
11fa52a0fdc0: Pull complete
Digest: sha256:30dfa439718a17baafefadf16c5e7c9d0a1cde97b4fd84f63b69e13513be7097
Status: Downloaded newer image for nginx:latest
Creating nginx ... done
```

Verify that nginx is reachable

```bash
$ curl -I http://localhost
HTTP/1.1 200 OK
Server: nginx/1.17.10
Date: Sat, 16 May 2020 03:58:28 GMT
Content-Type: text/html
Connection: keep-alive
```

